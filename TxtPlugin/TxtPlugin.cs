﻿using System.IO;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using ITSK_Test.Code.PlugIns;

namespace TxtPlugin
{
    [Export(typeof(IFilePlugins))]
    public class TxtPlugin : IFilePlugins
    {
        public string ParameterValue { get; set; }

        public TxtPlugin () { }
        public TxtPlugin(string substring)
        {
            ParameterValue = substring;
        }

        public void SetNewValue(string newValue)
        {
            ParameterValue = newValue;
        }

        public string GetFileType()
        {
            return ".txt";
        }
        public string GetParameterName()
        {
            return "Substring";
        }

        public async Task<bool> IsValid(FileInfo file)
        {
            if (file.Name.EndsWith(GetFileType()) && !string.IsNullOrEmpty(ParameterValue))
            {
                try
                {
                    using (var sr = file.OpenText())
                    {
                        var contents = await sr.ReadToEndAsync();
                        return contents.Contains(ParameterValue);
                    }
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
