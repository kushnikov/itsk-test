using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace ITSK_Test
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).UseUrls("http://*:5001/").Build();

            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
