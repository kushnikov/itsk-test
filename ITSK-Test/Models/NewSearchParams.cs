﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITSK_Test.Models
{
    public class NewSearchParams
    {
        public string Path { get; set; }
        public bool IsSearchIsSubDirectories { get; set; }
        public long? Size { get; set; }
        public bool Hidden { get; set; }
        public bool ReadOnly { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string Plugin { get; set; }
        public string ParameterValue { get; set; }
    }
}
