﻿namespace ITSK_Test.Models
{
    public class PluginInfo
    {
        public string Type { get; set; }
        public string FileType { get; set; }
        public string ParameterName { get; set; }
    }
}
