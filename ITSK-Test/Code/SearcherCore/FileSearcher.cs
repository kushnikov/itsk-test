﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ITSK_Test.Models;

namespace ITSK_Test.Code.SearcherCore
{
    public class FileSearcher
    {
        private readonly ConcurrentBag<Task> _taskHandlers;
        private readonly string _folder;
        private readonly bool _suppressOperationCanceledException;
        private readonly ExecuteHandlers _handlerOption;
        private readonly bool _searchInSubdirectories;
        private readonly CancellationTokenSource _tokenSource;

        public Func<FileInfo, bool> IsValid;

        public event EventHandler<FileEventArgs> FilesFound;
        public event EventHandler<SearchCompletedEventArgs> SearchCompleted;

        public FileSearcher(NewSearchParams parameters , CancellationTokenSource tokenSource)
        {
            _folder = parameters.Path;
            _suppressOperationCanceledException = true;
            _handlerOption = ExecuteHandlers.InCurrentTask;
            _searchInSubdirectories = parameters.IsSearchIsSubDirectories;
            _taskHandlers = new ConcurrentBag<Task>();
            _tokenSource = tokenSource;
        }

        public FileSearcher(string folder, Func<FileInfo, bool> isValid, CancellationTokenSource tokenSource, 
            bool suppressOperationCanceledException = true, ExecuteHandlers handlerOptions = ExecuteHandlers.InCurrentTask, bool searchInSubdirectories = true)
        {
            _folder = folder;
            _tokenSource = tokenSource;
            IsValid = isValid;
            _suppressOperationCanceledException = suppressOperationCanceledException;
            _handlerOption = handlerOptions;
            _searchInSubdirectories = searchInSubdirectories;
            _taskHandlers = new ConcurrentBag<Task>();
        }

        public Task StartSearchAsync()
        {
            return Task.Run(() => StartSearch(), _tokenSource.Token);
        }

        public void StartSearch()
        {
            try
            {
                GetFilesFast();
            }
            catch (OperationCanceledException)
            {
                OnSearchCompleted(true);

                if (!_suppressOperationCanceledException)
                    _tokenSource.Token.ThrowIfCancellationRequested();

                return;
            }

            OnSearchCompleted(false);
        }

        public void StopSearch()
        {
            _tokenSource.Cancel();
        }

        private void CallFilesFound(List<FileInfo> files)
        {
            var handler = FilesFound;

            if (handler == null) return;

            var arg = new FileEventArgs(files);
            handler(this, arg);
        }

        private void OnFilesFound(List<FileInfo> files)
        {
            if (_handlerOption == ExecuteHandlers.InNewTask)
            {
                _taskHandlers.Add(Task.Run(() => CallFilesFound(files)));
            }
            else
            {
                CallFilesFound(files);
            }
        }

        private void OnSearchCompleted(bool isCanceled)
        {
            if (_handlerOption == ExecuteHandlers.InNewTask)
            {
                try
                {
                    Task.WaitAll(_taskHandlers.ToArray());
                }
                catch (AggregateException exception)
                {
                    if (!(exception.InnerException is TaskCanceledException))
                        throw;

                    if (!isCanceled)
                        isCanceled = true;
                }

                CallSearchCompleted(isCanceled);
            }
            else
            {
                CallSearchCompleted(isCanceled);
            }
        }

        private void CallSearchCompleted(bool isCanceled)
        {
            var handler = SearchCompleted;

            if (handler == null) return;

            var arg = new SearchCompletedEventArgs(isCanceled);

            handler(this, arg);
        }

        private List<DirectoryInfo> GetStartDirectories(string folder)
        {
            _tokenSource.Token.ThrowIfCancellationRequested();

            DirectoryInfo[] directories;
            var resultFiles = new List<FileInfo>();

            try
            {
                var dirInfo = new DirectoryInfo(folder);
                directories = dirInfo.GetDirectories();

                var files = dirInfo.GetFiles();

                resultFiles.AddRange(files.Where(file => IsValid(file)));

                if (resultFiles.Count > 0)
                    OnFilesFound(resultFiles);

                if (directories.Length > 1)
                    return new List<DirectoryInfo>(directories);

                if (directories.Length == 0)
                    return new List<DirectoryInfo>();
            }
            catch (UnauthorizedAccessException)
            {
                return new List<DirectoryInfo>();
            }
            catch (PathTooLongException)
            {
                return new List<DirectoryInfo>();
            }
            catch (DirectoryNotFoundException)
            {
                return new List<DirectoryInfo>();
            }

            return GetStartDirectories(directories[0].FullName);
        }

        private void GetFiles(string folder)
        {
            _tokenSource.Token.ThrowIfCancellationRequested();

            DirectoryInfo dirInfo;
            DirectoryInfo[] directories;
            var resultFiles = new List<FileInfo>();

            try
            {
                dirInfo = new DirectoryInfo(folder);
                directories = dirInfo.GetDirectories();

                if (directories.Length == 0 || !_searchInSubdirectories)
                {
                    var files = dirInfo.GetFiles();
                    
                    resultFiles.AddRange(files.Where(file => IsValid(file)));

                    if (resultFiles.Count > 0)
                        OnFilesFound(resultFiles);

                    return;
                }
            }
            catch (UnauthorizedAccessException)
            {
                return;
            }
            catch (PathTooLongException)
            {
                return;
            }
            catch (DirectoryNotFoundException)
            {
                return;
            }

            foreach (var dir in directories)
            {
                _tokenSource.Token.ThrowIfCancellationRequested();
                GetFiles(dir.FullName);
            }

            _tokenSource.Token.ThrowIfCancellationRequested();

            try
            {
                var files = dirInfo.GetFiles();

                resultFiles.AddRange(files.Where(file => IsValid(file)));

                if (resultFiles.Count > 0)
                    OnFilesFound(resultFiles);
            }
            catch (UnauthorizedAccessException)
            {
            }
            catch (PathTooLongException)
            {
            }
            catch (DirectoryNotFoundException)
            {
            }
        }

        private void GetFilesFast()
        {
            if (_searchInSubdirectories)
            {
                var startDirs = GetStartDirectories(_folder);

                startDirs.AsParallel().WithCancellation(_tokenSource.Token).ForAll(d =>
                {
                    GetStartDirectories(d.FullName).AsParallel().WithCancellation(_tokenSource.Token).ForAll(dir =>
                    {
                        GetFiles(dir.FullName);
                    });
                });
            }
            else
            {
                GetFiles(_folder);
            }
        }
    }
}
