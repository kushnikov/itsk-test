﻿using System;

namespace ITSK_Test.Code.SearcherCore
{
    public class SearchCompletedEventArgs : EventArgs
    {
        public bool IsCanceled { get; private set; }

        public SearchCompletedEventArgs(bool isCanceled)
        {
            IsCanceled = isCanceled;
        }
    }
}
