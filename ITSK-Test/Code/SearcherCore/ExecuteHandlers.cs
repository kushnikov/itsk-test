﻿namespace ITSK_Test.Code.SearcherCore
{
    public enum ExecuteHandlers
    {
        InCurrentTask = 0,
        InNewTask = 1
    }
}
