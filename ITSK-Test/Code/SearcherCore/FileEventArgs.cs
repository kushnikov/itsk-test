﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ITSK_Test.Code.SearcherCore
{
    public class FileEventArgs : EventArgs
    {
        public List<FileInfo> Files { get; private set; }

        public FileEventArgs(List<FileInfo> files)
        {
            Files = files;
        }
    }
}
