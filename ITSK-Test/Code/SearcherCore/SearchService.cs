﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ITSK_Test.Code.Hubs;
using ITSK_Test.Code.PlugIns;
using ITSK_Test.Models;
using Microsoft.AspNetCore.SignalR;

namespace ITSK_Test.Code.SearcherCore
{
    public class SearchService
    {
        private readonly IHubContext<SearchHub> _hubContext;
        private readonly IPluginsRepo _pluginsRepo;
        private static object _locker = new object(); // locker object
        private FileSearcher _searcher;

        public bool Status;
        public List<FileInfo> Files = new List<FileInfo>();


        public SearchService(IHubContext<SearchHub> hubContext, IPluginsRepo pluginsRepo)
        {
            _hubContext = hubContext;
            _pluginsRepo = pluginsRepo;
        }

        public async Task StartSearch(NewSearchParams parameters)
        {
            if (!Directory.Exists(parameters.Path))
            {
                await _hubContext.Clients.All.SendAsync("ShowMessage", "Directory doesn't exist");
                return;
            }

            if (string.IsNullOrEmpty(parameters.Plugin))
            {
                await _hubContext.Clients.All.SendAsync("ShowMessage", "You should choose plugin");
                return;
            }

            var plugin = _pluginsRepo.GetFilePlugins().First(p => p.GetType().Name == parameters.Plugin);

            plugin.SetNewValue(parameters.ParameterValue);

            _searcher?.StopSearch();
            Files = new List<FileInfo>();

            var tokenSource = new CancellationTokenSource();
            _searcher = new FileSearcher(parameters, tokenSource)
            {
                IsValid = fileInfo =>
                {
                    if (parameters.Size.HasValue && parameters.Size.Value != fileInfo.Length) return false;
                    if (parameters.Hidden && !fileInfo.Attributes.HasFlag(FileAttributes.Hidden)) return false;
                    if (parameters.ReadOnly && !fileInfo.Attributes.HasFlag(FileAttributes.ReadOnly)) return false;
                    if (parameters.DateFrom.HasValue && parameters.DateFrom.Value < fileInfo.CreationTime) return false;
                    if (parameters.DateTo.HasValue && parameters.DateTo.Value > fileInfo.CreationTime) return false;
                    return plugin.IsValid(fileInfo).Result;
                }
            };

            _searcher.FilesFound += async (sender, eventArgs) =>
            {
                var onlyThisFiles = new List<FileInfo>();
                lock (_locker)
                {
                    eventArgs.Files.ForEach((f) =>
                    {
                        onlyThisFiles.Add(f);
                    });

                    Files.AddRange(onlyThisFiles);
                }

                await _hubContext.Clients.All.SendAsync("NewPartOfFiles", onlyThisFiles.Select(f => f.FullName));
            };

            _searcher.SearchCompleted += async (sender, eventArgs) =>
            {
                Status = false;
                await _hubContext.Clients.All.SendAsync("ChangeStatus", Status);

                if (eventArgs.IsCanceled)
                    await _hubContext.Clients.All.SendAsync("ShowMessage", "Search stopped");
                else
                    await _hubContext.Clients.All.SendAsync("ShowMessage", "Search completed");

            };

            _searcher.StartSearchAsync();
            Status = true;
            await _hubContext.Clients.All.SendAsync("ChangeStatus", Status);

        }

        public void StopSearch()
        {
            if (_searcher != null && Status)
                _searcher.StopSearch();
        }


    }
}
