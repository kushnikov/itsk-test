﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using ITSK_Test.Code.Hubs;
using ITSK_Test.Models;
using Microsoft.AspNetCore.SignalR;

namespace ITSK_Test.Code.PlugIns
{
    public class PluginsRepo : IPluginsRepo
    {
        private bool _started;

        private static readonly DirectoryCatalog Catalog = new DirectoryCatalog(Environment.CurrentDirectory + @"\plugins");
        private static readonly CompositionContainer Container = new CompositionContainer(Catalog);
        private static readonly FileSystemWatcher Watcher = new FileSystemWatcher();
        private readonly IHubContext<SearchHub> _hub;

        [ImportMany(AllowRecomposition = true)]
        public IFilePlugins[] FilePlugins;

        public IFilePlugins[] GetFilePlugins()
        {
            return FilePlugins;
        }

        public PluginsRepo(IHubContext<SearchHub> hub)
        {
            _hub = hub;
            StartCompose();
        }

        public void StartCompose()
        {
            if(_started) return;
            Container.ComposeParts(this);
            Watcher.Path = Environment.CurrentDirectory + @"\plugins";

            Watcher.NotifyFilter = NotifyFilters.Size | NotifyFilters.FileName;
            Watcher.Created += OnChanged;
            Watcher.Deleted += OnChanged;
            Watcher.Filter = "*.dll";
            Watcher.EnableRaisingEvents = true;
            _started = true;
        }


        private void OnChanged(object source, FileSystemEventArgs e)
        {
            // TODO find reason of multiple event running
            Catalog.Refresh();
            _hub.Clients.All.SendAsync("GetPlugins", FilePlugins.Select(p => new PluginInfo()
            {
                FileType = p.GetFileType(),
                ParameterName = p.GetParameterName(),
                Type = p.GetType().Name
            }));
        }
            

        public void StopCompose()
        {
            Watcher.EnableRaisingEvents = false;
        }

    }
}
