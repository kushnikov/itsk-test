﻿using System.IO;
using System.Threading.Tasks;

namespace ITSK_Test.Code.PlugIns
{
    public interface IFilePlugins
    {
        Task<bool> IsValid(FileInfo file);
        void SetNewValue(string newValue);
        string GetFileType();
        string GetParameterName();
    }
}
