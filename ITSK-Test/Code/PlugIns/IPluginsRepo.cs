﻿namespace ITSK_Test.Code.PlugIns
{
    public interface IPluginsRepo
    {
        void StartCompose();
        void StopCompose();
        IFilePlugins[] GetFilePlugins();
    }
}
