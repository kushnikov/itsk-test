﻿using System.Linq;
using System.Threading.Tasks;
using ITSK_Test.Code.PlugIns;
using ITSK_Test.Code.SearcherCore;
using ITSK_Test.Models;
using Microsoft.AspNetCore.SignalR;

namespace ITSK_Test.Code.Hubs
{
    public class SearchHub : Hub
    {
        private readonly IPluginsRepo _pluginsRepo;
        private readonly SearchService _searchService;
        public SearchHub(IPluginsRepo pluginsRepo, SearchService searchService)
        {
            _pluginsRepo = pluginsRepo;
            _searchService = searchService;
        }

        public override async Task OnConnectedAsync()
        {
            var pluginsInfo = _pluginsRepo.GetFilePlugins()
                .Select(p => new PluginInfo()
                {
                    FileType = p.GetFileType(),
                    ParameterName = p.GetParameterName(),
                    Type = p.GetType().Name
                });
            await Clients.Caller.SendAsync("GetPlugins", pluginsInfo);
            await Clients.Caller.SendAsync("ChangeStatus", _searchService.Status);
            if (_searchService.Status) await Clients.Caller.SendAsync("NewPartOfFiles", _searchService.Files);
            await base.OnConnectedAsync();
        }

        public async Task StartSearch(NewSearchParams parameters)
        {
            await _searchService.StartSearch(parameters);
        }

        public void StopSearch()
        {
            _searchService.StopSearch();
        }
    }
}
