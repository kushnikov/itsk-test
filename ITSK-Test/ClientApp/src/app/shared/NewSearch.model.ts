export class NewSearch{
    constructor(
        public path: string,
        public isSearchIsSubDirectories: boolean,
        public size: number,
        public hidden: boolean,
        public readOnly: boolean,
        public dateFrom: Date,
        public dateTo: Date,
        public plugin: string,
        public parameterValue: string
    ) { }
}