export class Plugin{
    public constructor(
        public fileType: string,
        public parameterName: string,
        public type: string
    ){}
}