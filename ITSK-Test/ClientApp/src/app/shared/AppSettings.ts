import { Injectable } from '@angular/core';

@Injectable()
export class AppSettings {
    public getUrl(): string{
        return this.baseUrl;
    }
    public readonly baseUrl = 'http://localhost:5001';
}