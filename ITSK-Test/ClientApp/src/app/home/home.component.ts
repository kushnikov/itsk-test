import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import * as signalR from "@aspnet/signalr";

import { NewSearch } from '../shared/NewSearch.model';
import { Plugin } from '../shared/Plugins.model';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  private connection: signalR.HubConnection;
  public form: FormGroup;
  public maxDate = new Date();
  public plugins: Array<Plugin>;
  public files: Array<string> = [];

  public isSearching = true;

  constructor(public formBuilder: FormBuilder, private snackBar: MatSnackBar) { 
    this.connectToHub();
  }

  connectToHub() {
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl("http://localhost:5001/hub")
      .build();

    this.connection.on("GetPlugins", (plugins: Array<Plugin>) => {
      this.plugins = plugins;
      console.log(this.plugins);
    });

    this.connection.on("GetNewPluginsArray", (pluginsArray) => {
      console.log(pluginsArray);
    });

    this.connection.on("ShowMessage", (message: string) => {
      this.snackBar.open(message, 'ok', {duration: 2000,});
    });

    this.connection.on("NewPartOfFiles", (files: Array<string>) => {
      this.files = this.files.concat(files);
    });

    this.connection.on("ChangeStatus", (status: boolean) => {
      this.isSearching = status;
      console.log("status changing: ", this.isSearching);
    });

    //this.connection.onclose

    this.connection.start().catch(err => console.log(err));
  }

  selectedPlugin(): Plugin {
    if (this.plugins == undefined || this.plugins == null) return null;
    return this.plugins.find(
      (s) => {
      return s.type === this.form.controls["plugin"].value;
      }
    );
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'path': ['', Validators.required],
      'isSearchIsSubDirectories': [true, Validators.required],
      'size': ['', Validators.pattern("^[0-9]*$")],
      'hidden': [false, Validators.required],
      'readOnly': [false, Validators.required],
      'dateFrom': ['', Validators.max(this.maxDate.getTime())],
      'dateTo': ['', Validators.max(this.maxDate.getTime())],
      'plugin': ['', Validators.required],
      'parameterValue': ['', null]
    });
  }

  onSubmit(newSearch: NewSearch){
    this.connection.invoke("StartSearch", newSearch);
  }

  onChangeSelectedPlugin(value: string){
    console.log(value);
  }

  onStopClick(event: Event){
    this.connection.invoke("StopSearch");
  }
}
