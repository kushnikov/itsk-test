﻿using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using ITSK_Test.Code.PlugIns;

namespace DllPlugin
{
    [Export(typeof(IFilePlugins))]
    public class DllPlugin : IFilePlugins
    {
        public string ParameterValue { get; set; }

        public DllPlugin() { }
        public DllPlugin(string name)
        {
            ParameterValue = name;
        }

        public void SetNewValue(string newValue)
        {
            ParameterValue = newValue;
        }

        public string GetFileType()
        {
            return ".dll";
        }
        public string GetParameterName()
        {
            return "Interface name";
        }

        public async Task<bool> IsValid(FileInfo file)
        {
            if (file.Name.EndsWith(GetFileType()) && !string.IsNullOrEmpty(ParameterValue))
            {
                try
                {
                    var asm = Assembly.LoadFrom(file.FullName);

                    return asm
                        .GetTypes().Any(t => t.GetInterfaces().Any(i => i.FullName.EndsWith(ParameterValue)));
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
